## Motorguide Styles

These are baseline styles for Motorguide.

Clone this repository and run `npm install`, followed by `npm start`. It should open up http://localhost:1234/ in your browser.

To deploy using Surge, run `npm run build`, followed by `surge`. Then, specify the `dist/` directory as the project root, and set the domain name (to motorguide-styles.surge.sh).

View the live version at [http://motorguide-styles.surge.sh]('http://motorguide-styles.surge.sh')